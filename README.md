# getTagAttr

#### 介绍
php匹配字符串中的指定标签属性的composer包

#### 软件架构
软件架构说明

#### 使用说明

        $url = 'https://www.mi.com/index.html';
        $html = file_get_contents($url);
        //以上是抓取网页图片，可根据需求使用指定字符串内容也可获取指定标签指定属性  "asjkdajkdjal哈哈哈哈哈哈<img src='http://xxxxxxxx.jpg' />哈啊哈"
        $pic = new PicService($html, 'img', 'src');
        $pic->getMatches();
        $result = $pic->getMatchesList();
        dump($result);

